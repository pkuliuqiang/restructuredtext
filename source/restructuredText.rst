reStructuredText简介
=======================

章节
---------

章节的表体在双上划线符号之间，并且符号的长度不能小于文本的长度：

	===================
	This is a heading
	===================


通常没有专门的符号表示标题的等级，但是对于Python 文档，可以这样认为:

* ``#`` 及上划线表示部分
* ``*`` 及上划线表示章节
* ``=``, 小章节
* ``-``, 子章节
* ``^``, 子章节的子章节
* ``"``, 段落


段落
----------------------

**段落** 是由空行分隔的一段文本。

内联标记
----------------------

*text* 是强调（斜体）
**text** 重点强调（加粗）
``text`` 代码样式

列表与引用
----------------------

列表标记，仅在段落的开头放置一个星号和一个缩进。

也可以使用符号``#``自动加序号。

* 这是一个项目符号列表
* 它有两项，
  第二项使用两行。

1. 这是一个有序列表
2. 也有两项
   
#. 是个有序列表。
#. 也有两项。
   
* 这是
* 一个列表
  
  * 嵌套列表
  * 子项

* 父列表继续

定义列表

术语(term文本开头行)
	定义术语，必须缩进

	可以由多段组成

下一术语(term)
	描述

无序列表

和顺序列表相似，使用“*”、“+”、“-”代替数字：

* 列表第一级

  + 第二级

    - 第三级

  + 第二级的另一个项目
  + 

源代码
-------------

字面代码块在段落的后面使用标记::引出。代码块必须缩进（同段落，需要与周伟文本以空行分割）：

这是一段郑长文本。下一段是代码文字::

	它不需要特别处理，仅是
	缩进就可以了.

	它可以有多行.

再是正常的文本段.

高亮代码
------------

可以用 .. code-block::追加各种语法高亮声明：

.. code-block:: python
	:linenos:

	def foo():
		print "Love Python, Love FreeDome"
		print "E文标点，.0123456789，中文标点."

引用外部代码：

.. .. literalinclude:: example.py
	:language: python





表格
----------

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======


CSV表格
----------

.. csv-table:: Frozen Delights!
	:header: "Treat", "Quantity", "Description"
	:widths: 15, 10, 30

	"Albatross", 2.99, "On a stick!"
	 "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be crunchy, now would it?"
	 "Gannet Ripple", 1.99, "On a stick!"


List表格
------------

.. list-table:: Weather forecast
	:header-rows: 1
	:widths: 7 7 7 7 60
	:stub-columns: 1

	* - Day
	  - Min Temp
	  - Max Temp
	  -
	  - Summ
	* - Monday
	  - 11C
	  - 22C
	  - .. image:: _static/sunny.jpg
	       :width: 30
	  - A clear day with lots of sunshine.
	    However, the strong breeze will bring
	    down the temperatures.
	* - Tuesday
	  - 9C
	  - 10C
	  -
	  -


超链接
----------


使用 `链接地址 <http://example.com/>`_ 可以插入网页链接。链接文本是网址，则不需要特别标记，分析器会自动发现文本里的链接或邮件地址.

可以把链接和标签分开，如下：

段落里包含 `链接`_.

.. _链接: http://www.baidu.com







